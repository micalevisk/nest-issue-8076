import { Module, OnModuleInit } from '@nestjs/common';
import { HttpAdapterHost, NestFactory } from '@nestjs/core';
import { Test } from '@nestjs/testing';

let context: any;

@Module({
  providers: [
    {
      provide: 'Test',
      inject: [HttpAdapterHost],
      useFactory: (adapterHost: HttpAdapterHost) => {
        console.info({
          context,
          'Has `httpAdapterHost.httpAdapter`?': !!adapterHost.httpAdapter,
        });

        return 'Test';
      },
    },
  ],
})
class AppModule implements OnModuleInit {
  constructor(private readonly adapterHost: HttpAdapterHost) {}
  onModuleInit(): void {
    console.info({
      context: 'onModuleInit',
      'Has `httpAdapterHost.httpAdapter`?': !!this.adapterHost.httpAdapter,
    });
  }
}

async function bootstrap() {
    context = 'NestFactory.create ("Test" provider)';
    const app = await NestFactory.create(AppModule, { logger: false });

    context = 'Test.createTestingModule ("Test" provider)';
    const testModule = await Test.createTestingModule({
        imports: [AppModule],
    }).compile();
    // await testModule.init();

    const testApp = testModule.createNestApplication();
    await testApp.init();
}
bootstrap();
