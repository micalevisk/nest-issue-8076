## Installation

```bash
npm install
```

## Running the app

```bash
# development
npm run start

# watch mode
npm run start:dev

# production mode
npm run start:prod
```

You'll get this output:

```
{
  context: 'NestFactory.create ("Test" provider)',
  'Has `httpAdapterHost.httpAdapter`?': true
}
{
  context: 'Test.createTestingModule ("Test" provider)',
  'Has `httpAdapterHost.httpAdapter`?': false
}
{ context: 'onModuleInit', 'Has `httpAdapterHost.httpAdapter`?': true }
```
